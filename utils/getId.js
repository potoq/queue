'use strict';

const generate = require('nanoid/generate');

module.exports = (n = 5) => generate('1234567890abcdefghigklmnop', n);
