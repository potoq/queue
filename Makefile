DC_SERVER_NAME := potoq-queue-server
DC_EDITOR_NAME := potoq-swagger-editor
DC_DB_NAME := potoq-queue-db
DC_NETWORK_NAME := potoq-network

start:
	@docker run --rm -it \
	--name $(DC_SERVER_NAME) \
	--user 1000:1000 \
	--network $(DC_NETWORK_NAME) \
	-v $(CURDIR):/local/project \
	-w /local/project \
	-p 10010:10010 \
	-e DB_HOST='$(DC_DB_NAME)' \
	node:10-alpine \
	npm start

server_exec:
	@docker exec -it $(DC_SERVER_NAME) ash

db:
	@docker start $(DC_DB_NAME)

db_purge: db_rm db_volume_rm

db_exec:
	@docker exec -it $(DC_DB_NAME) redis-cli

db_run:
	@docker run -d --name $(DC_DB_NAME) \
	--network $(DC_NETWORK_NAME) \
	--mount source=$(DC_DB_NAME),target=/data \
	-p 6379:6379 \
	redis \
	redis-server --appendonly yes

db_stop:
	@docker stop $(DC_DB_NAME)

db_rm: db_stop
	@docker rm $(DC_DB_NAME)

db_volume_rm:
	@docker volume rm $(DC_DB_NAME)

editor:
	@docker start $(DC_EDITOR_NAME)

editor_run:
	@docker run -d --name $(DC_EDITOR_NAME) \
	--user 1000:1000 \
	-v $(CURDIR):/local/project \
	-w /local/project \
	-p 4444:4444 \
	node:10-alpine \
	npm run editor

editor_stop:
	@docker stop $(DC_EDITOR_NAME)

editor_rm: editor_stop
	@docker rm $(DC_EDITOR_NAME)