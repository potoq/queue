'use strict';

const fs = require('fs');
const path = require('path');
const http = require('http');

const app = require('connect')();
const swaggerTools = require('swagger-tools');
const jsyaml = require('js-yaml');
const { config, redis } = require('./config');

const spec = fs.readFileSync(path.join(__dirname, 'api/swagger.yaml'), 'utf8');
const swaggerDoc = jsyaml.safeLoad(spec);

if (process.env.HOST_IP) {
  swaggerDoc.host = `${process.env.HOST_IP}:${config.server.port}`;
} else {
  swaggerDoc.host = `localhost:${config.server.port}`;
}

let server;

redis
  .on('ready', () => {
    swaggerTools.initializeMiddleware(swaggerDoc, middleware => {
      app.use(middleware.swaggerMetadata());
      app.use(middleware.swaggerValidator());
      app.use(
        middleware.swaggerRouter({
          swaggerUi: path.join(__dirname, '/swagger.json'),
          controllers: path.join(__dirname, './controllers'),
          useStubs: process.env.NODE_ENV === 'development' // Conditionally turn on stubs (mock mode)
        })
      );
      app.use(middleware.swaggerUi());

      server = http.createServer(app);
      server.listen(config.server.port, () => {
        console.log(
          'Your server is listening on port %d (http://localhost:%d)',
          config.server.port,
          config.server.port
        );
        console.log(
          'Swagger-ui is available on http://localhost:%d/docs',
          config.server.port
        );
      });
    });
  })
  .on('error', error => {
    if (server) {
      server.close();
    }
    console.error(error);
    process.exit(1);
  });
