'use strict';

const Redis = require('ioredis');
const { redis } = require('./config');

Redis.Command.setReplyTransformer('hgetall', result => {
  if (Array.isArray(result)) {
    const obj = {};
    for (let i = 0; i < result.length; i += 2) {
      obj[result[i]] = result[i + 1];
    }
    return obj;
  }
  return result;
});

module.exports = new Redis(redis);
