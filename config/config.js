'use strict';

const config = {
  server: {
    port: 10010
  },
  redis: {}
};

if (process.env.DB_HOST) {
  config.redis['host'] = process.env.DB_HOST;
}

module.exports = config;
