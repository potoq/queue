'use strict';

const utils = require('../utils/writer');
const UserService = require('../service/UserService');

module.exports = {
  addUser,
  getUsers,
  changeUserCoin,
  getUserCoin
};

function addUser(req, res) {
  const newUser = req.swagger.params['newUser'].value;
  UserService.addUser(newUser)
    .then(() => {
      utils.writeJson(res);
    })
    .catch(response => {
      utils.writeJson(res, response);
    });
}

function getUsers(req, res) {
  UserService.getUsers()
    .then(users => {
      utils.writeJson(res, { users });
    })
    .catch(response => {
      utils.writeJson(res, response);
    });
}

function changeUserCoin(req, res) {
  const userId = req.swagger.params['userId'].value;
  const { coin } = req.swagger.params['newCoin'].value;

  UserService.changeUserCoin(userId, coin)
    .then(() => {
      utils.writeJson(res);
    })
    .catch(response => {
      utils.writeJson(res, response);
    });
}

function getUserCoin(req, res) {
  const userId = req.swagger.params['userId'].value;
  UserService.getUserCoin(userId)
    .then(coin => {
      utils.writeJson(res, { coin });
    })
    .catch(response => {
      utils.writeJson(res, response);
    });
}
