'use strict';

const utils = require('../utils/writer');
const Branch = require('../service/BranchService');

function addBranchAsString(req, res) {
  const newBranch = req.swagger.params['newBranch'].value;
  Branch.addBranchAsString(newBranch)
    .then(idBranch => {
      utils.writeJson(res, idBranch);
    })
    .catch(response => {
      utils.writeJson(res, response);
    });
}

function getStringBranchById(req, res) {
  const branchId = req.swagger.params['branchId'].value;
  Branch.getStringBranchById(branchId)
    .then(result => {
      utils.writeJson(res, { result });
    })
    .catch(response => {
      utils.writeJson(res, response);
    });
}

function getBranches(req, res) {
  Branch.getBranches()
    .then(branches => {
      utils.writeJson(res, { branches });
    })
    .catch(response => {
      utils.writeJson(res, response);
    });
}

module.exports = {
  addBranchAsString,
  getStringBranchById,
  getBranches
};
