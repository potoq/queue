'use strict';

const { redis } = require('../config');

const USER_PREFIX = 'user';

exports.addUser = addUser;
exports.getUsers = getUsers;
exports.changeUserCoin = changeUserCoin;
exports.getUserCoin = getUserCoin;

function addUser({ userId }) {
  return new Promise((res, rej) => {
    redis
      .multi()
      .sadd(`${USER_PREFIX}s`, userId)
      .hmset(`${USER_PREFIX}:${userId}`, { userId })
      .set(`${USER_PREFIX}:${userId}:coin`, 0)
      .exec(err => {
        if (err) rej(err);
        res();
      });
  });
}

function getUsers() {
  return redis.smembers(`${USER_PREFIX}s`);
}

async function changeUserCoin(userId, newCoinValue) {
  const hasUser = await checkUser(userId);
  if (!hasUser) {
    await addUser({ userId });
  }

  return redis.set(`${USER_PREFIX}:${userId}:coin`, newCoinValue);
}

function getUserCoin(userId) {
  return redis.get(`${USER_PREFIX}:${userId}:coin`);
}

function checkUser(userId) {
  return redis.sismember(`${USER_PREFIX}s`, userId);
}
