'use strict';

const { redis } = require('../config');
const { getId } = require('../utils');

exports.getBranches = getBranches;
exports.addBranchAsString = addBranchAsString;
exports.getStringBranchById = getStringBranchById;

const ID_LEN = 5;

function getBranches() {
  return redis.smembers('branches');
}

function addBranchAsString(newBranch) {
  const idBranch = getId(ID_LEN);

  return new Promise((res, rej) => {
    redis
      .multi()
      .set(`branch:s:${idBranch}`, newBranch)
      .sadd('branches', idBranch)
      .exec((err, { userId }) => {
        if (err) rej(err);
        res({ userId });
      });
  });
}

function getStringBranchById(branchId) {
  return redis.get(`branch:s:${branchId}`);
}
