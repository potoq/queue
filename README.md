# Potoq Queue API Service

## Dev Overview

Redis port = `6379`

Server port = `10010`

UI API = `http://localhost:10010/docs`

### Быстрый запуск

```
docker-compose up
```

### Redis

Создать контейнер с Redis

```
make db_run
```

Остановить контейнер с Redis

```
make db_stop
```

Запустить redis-cli

```
make db_exec
```

Удалить контейнер с Redis

```
make db_rm
```

### Запуск сервера (nodemon)

```
npm start
```

### Запустить сервер в контейнере

```
make start
```

Зайти в контейнер с сервером

```
make server_exec
```
